import { User } from "./user";
import { Credit } from './credit';

export interface ServerValidationError {
    statusCode: number;
    error: string;
    message: Message[];
}

export interface Message {
    target: Target;
    value: number;
    property: string;
    children: any[];
    constraints: Constraints;
}

export interface Constraints {
    message: string;
}

export type Target = User | Credit;
