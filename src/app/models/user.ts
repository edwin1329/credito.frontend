export interface User {
    id?: number;
    first_name: string;
    last_name: string;
    document: number;
    birth_date: string;
    password: string;
}
