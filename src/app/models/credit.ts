export interface Credit {
    nit: string;
    company: string;
    salary: number;
    employed_at: string;
}
