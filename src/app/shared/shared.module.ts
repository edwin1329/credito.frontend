import { NgModule } from '@angular/core';
import { ErrorListComponent } from './components/error-list/error-list.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ErrorListComponent
  ],
  exports: [
    ErrorListComponent
  ]
})
export class SharedModule { }