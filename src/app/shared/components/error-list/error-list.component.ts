import { Component, Input } from '@angular/core';
import { ServerValidationError } from 'src/app/models/server-validation-error';

@Component({
    selector: 'error-list',
    templateUrl: './error-list.component.html'
})
export class ErrorListComponent {
    @Input() errors: string | ServerValidationError;
}
