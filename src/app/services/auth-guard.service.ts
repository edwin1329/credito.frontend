import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { UserService } from './user.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private userService: UserService) { }

    canActivate() {
        if (this.userService.isAuthenticated()) {
            return true;
        }

        this.router.navigate(['/']);
        return false;
    }
}