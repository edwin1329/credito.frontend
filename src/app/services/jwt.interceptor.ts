import {
    HttpErrorResponse,
    HttpHandler,
    HttpHeaderResponse,
    HttpInterceptor,
    HttpProgressEvent,
    HttpRequest,
    HttpResponse,
    HttpSentEvent,
    HttpUserEvent
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserService } from './user.service';
import { Router } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(
        private _userService: UserService,
        private _routerExt: Router) {}

    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({ setHeaders: { Authorization: 'Bearer ' + token }});
    }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {

        if (req.url.indexOf('/auth') < 0) {
            return next.handle(this.addToken(req, this._userService.getToken())).pipe(
                catchError(error => {
                    if (error instanceof HttpErrorResponse) {
                        switch ((<HttpErrorResponse>error).status) {
                            case 401:
                                return this.logoutUser(req);
                            default:
                                return throwError(error);
                        }
                    } else {
                        return throwError(error);
                    }
                })
            );
        }

        return next.handle(req);
    }

    logoutUser(req: HttpRequest<any>) {
        this._userService.removeToken();

        if (req.url.indexOf('auth/login') < 0) {
            // Route to the login page
            this._routerExt.navigate(['/auth']);
        }

        return throwError({ status: 401, message: 'User must to login' });
    }
}
