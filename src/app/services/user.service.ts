import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError, Observable, of } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { User } from '../models/user';

@Injectable()
export class UserService {

  private url: string = 'http://localhost:3000/';

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  documentExists(document: number): Observable<boolean> {
    return this.http.get<User>(`${this.url}auth`, {
      params: { document: String(document) }
    }).pipe(
      map(user => user.id > 0),
      catchError(() => of(false))
    );
  }

  register(user: User) {
    return this.http.post<User>(`${this.url}auth/register`, user).pipe(
      catchError(error => {
        if (error.status === 400) {
          return throwError(error.error);
        }
      })
    );
  }

  login(user: User) {
    return this.http.post<{ access_token: string }>(`${this.url}auth/login`, user).pipe(
      tap(response => localStorage.setItem('access_token', response.access_token))
    );
  }

  logOut() {
    this.removeToken();
    this.router.navigate(['/']);
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  removeToken() {
    localStorage.removeItem('access_token')
  }

  isAuthenticated(): boolean {
    let token = this.getToken();
    // a token is present
    if (token) {
      // token with a valid JWT format XXX.YYY.ZZZ
      if (token.split('.').length === 3) {
        // could be a valid JWT or an access token with the same format
        try {
          let base64Url = token.split('.')[1];
          let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
          let exp = JSON.parse(window.atob(base64)).exp;
          // jwt with an optional expiration claims
          if (exp) {
            let isExpired = Math.round(new Date().getTime() / 1000) >= exp;
            if (isExpired) {
              // fail: Expired token
              this.removeToken();
              return false;
            } else {
              // pass: Non-expired token
              return true;
            }
          }
        } catch (e) {
          return false;
        }
      }
    }

    return false;
  }
}
