import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Credit } from '../models/credit';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class CreditService {

  private url: string = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  save(credit: Credit) {
    return this.http.post<Credit>(`${this.url}credit`, credit).pipe(
      catchError(error => {
        if (error.status == 400) {
          return throwError(error.error);
        }
      })
    );
  }
}
