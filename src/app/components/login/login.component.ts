import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

import { UserService } from 'src/app/services/user.service';
import { ServerValidationError } from 'src/app/models/server-validation-error';
import { User } from '../../models/user';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  user: User = {} as User;
  operation: string = 'login';
  maxDate = new Date();
  errors: ServerValidationError | string;
  documentExists$: Observable<boolean>;

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.documentExists$ = of(false);
  }

  isDate18orMoreYearsOld(date: Date) {
    date = new Date(date);
    return new Date(date.getFullYear() + 18, date.getMonth(), date.getDate()) <= new Date();
  }

  onDateChange(event: MatDatepickerInputEvent<Date>) {
    this.user.birth_date = this.isDate18orMoreYearsOld(event.value) ? event.value.toISOString() : null;
  }

  onBlurDocument(event: any) {
    const document = event.target.value;
    if (this.operation === 'register' && document) {
      this.documentExists$ = this.userService.documentExists(document).pipe(
        tap(exists => this.errors = exists ?
          'El documento ya existe en nuestra base de datos.' : null)
      );
    }
  }

  toggleForm(mode: string) {
    this.operation = mode;
    this.errors = null;
    this.documentExists$ = of(mode === 'register');
    if (mode === 'register') {
      this.user.document = null;
      this.user.password = null;
    }
  }

  login() {
    if (!this.isValidForm()) return;
    this.userService
        .login(this.user)
        .subscribe(
          user => this.router.navigate(['/credit']),
          (error) => this.errors = error.error.message
        );
  }

  register() {
    if (!this.isValidForm()) return;
    this.errors = null;
    this.userService
        .register(this.user)
        .subscribe(
          user => this.toggleForm('login'),
          (error: ServerValidationError) => this.errors = error
        );
  }

  private isValidForm() {
    let isValid = this.user.document && this.user.password;
    if (this.operation === 'register') {
      isValid = isValid && this.user.first_name && this.user.last_name && this.user.birth_date;
    }

    if (!isValid) {
      this.errors = 'Diligencie todos los campos';
    }

    return isValid;
  }

}
