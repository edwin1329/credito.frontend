import { Component, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Credit } from 'src/app/models/credit';
import { CreditService } from 'src/app/services/credit.service';
import { UserService } from 'src/app/services/user.service';
import { ServerValidationError } from '../../models/server-validation-error';

@Component({
  selector: 'app-credito',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {

  credit: Credit = {} as Credit;
  result: Credit;
  errors: ServerValidationError;
  maxDate = new Date();

  constructor(
    private userService: UserService,
    private creditService: CreditService
  ) { }

  ngOnInit() {
  }

  employedAt18MoreYearsOld(input: Date) {
    const date = new Date(input);
    date.setMonth(date.getMonth() + 18);

    return date <= new Date();
  }

  onDateChange(event: MatDatepickerInputEvent<Date>) {
    const date = event.value;
    this.credit.employed_at = date && this.employedAt18MoreYearsOld(date) ? date.toISOString() : null;
  }

  submit() {
    this.result = null;
    this.errors = null;
    this.creditService.save(this.credit)
      .subscribe(
        result => this.result = result,
        (error: ServerValidationError) => this.errors = error
      );
  }

  logOut() {
    this.userService.logOut();
  }
}
